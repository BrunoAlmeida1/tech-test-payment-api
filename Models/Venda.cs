using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DesafioPaymentApi.Models
{
    public class Venda
    {
        public Venda() {}
        public Venda(string item, StatusDaVenda status, int vendedorId)
        {
            Itens = item;
            Status = status;
            VendedorId = vendedorId;
        }
        public int VendaId { get; set; }
        public string Itens { get; set; }
        public StatusDaVenda Status { get; set; }
        public int VendedorId { get; set; }
        public Vendedor Vendedor { get; set; }
    }
}