using System.Text.Json.Serialization;

namespace DesafioPaymentApi.Models
{
    [JsonConverter(typeof(JsonStringEnumConverter))]
    public enum StatusDaVenda
    {
        AguardandoPagamento,
        PagamentoAprovado,
        EnviadoParaTransportadora,
        Entregue,
        Cancelada
    }
}