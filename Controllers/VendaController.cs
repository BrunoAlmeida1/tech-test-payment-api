using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using DesafioPaymentApi.Context;
using DesafioPaymentApi.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.JsonPatch;
namespace DesafioPaymentApi.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendaController : ControllerBase
    {
        private readonly VendaContext _context;

        public VendaController(VendaContext context)
        {
            _context = context;
        }

        [HttpPost]
        public IActionResult CadastrarVendedor(Vendedor vendedor)
        {
            _context.Add(vendedor);
            _context.SaveChanges();

            return Ok(vendedor);
        }

        [HttpPut]
        public IActionResult RegistrarVenda(Venda venda)
        {
            _context.Add(venda);
            _context.SaveChanges();
            _context.Vendas.Find(venda.VendaId).Status = 0;
            _context.SaveChanges();

            return Ok(venda);
        }

        [HttpGet("{id}")]
        public IActionResult ObterVendaPorId(int id)
        {
            if (_context.Vendas.Find(id) == null)
                return NotFound();

            var tabelaVendedor = _context.Vendedores.ToList();
            var tabelaVendas = _context.Vendas.ToList();

            var query =
                from vendedor in tabelaVendedor
                join venda in tabelaVendas on vendedor.VendedorId equals venda.VendedorId
                where venda.VendaId == id
                select new
                {
                    VendaId = venda.VendaId,
                    Itens = venda.Itens,
                    Status = venda.Status,
                    Vendedor = new
                    {
                        VendedorId = vendedor.VendedorId,
                        Nome = vendedor.Nome,
                        CPF = vendedor.Cpf,
                        Email = vendedor.Email,
                        Telefone = vendedor.Fone
                    }
                };

            return Ok(query);
        }

        [HttpPatch("{id}")]
        public IActionResult AtualizarStatus(int id, [FromBody] JsonPatchDocument<Venda> venda)
        {
            if (venda == null)
                return BadRequest();

            var vendaDatabase = _context.Vendas.Find(id);
            if (vendaDatabase == null)
                return NotFound();

            var statusDatabase = vendaDatabase.Status;

            venda.ApplyTo(vendaDatabase, ModelState);

            /* if (statusDatabase.Equals(0))
            {
                if (status.Equals(1) || status.Equals(4))
                    vendaDatabase.Status = status;
                else
                {
                    var mensagem = $"O status {statusDatabase} só pode ser alterado para Pagamento Aprovado ou Cancelada.";

                    return Ok(new { mensagem });
                }
            }
            else if (statusDatabase.Equals(1))
            {
                if (status.Equals(2) || status.Equals(4))
                    vendaDatabase.Status = status;
                else
                {
                    var mensagem = $"O status {statusDatabase} só pode ser alterado para Enviado Para Transportadora ou Cancelada.";

                    return Ok(new { mensagem });
                }
            }
            else if (statusDatabase.Equals(2))
            {
                if (status.Equals(3))
                    vendaDatabase.Status = status;
                else
                {
                    var mensagem = $"O status {statusDatabase} só pode ser alterado para Entregue.";

                    return Ok(new { mensagem });
                }
            } */
            _context.SaveChanges();

            return Ok(vendaDatabase);
        }
    }
}